const express = require('express')
const router = express.Router()
const ApplicationController = require('../controller/ApplicationController')

router.get('/', ApplicationController.getApplications)
router.get('/:id', ApplicationController.getApplication)
router.post('/', ApplicationController.addApplication)
router.put('/', ApplicationController.updateApplication)
router.delete('/:id', ApplicationController.deleteApplication)
router.get('/confirm/:id', ApplicationController.confirmApplication)
router.get('/decline/:id', ApplicationController.declineApplication)

module.exports = router
