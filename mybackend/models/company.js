const mongoose = require('mongoose')
const companySchema = new mongoose.Schema({
  companyname: String,
  address: String,
  email: String,
  tel: String
})

module.exports = mongoose.model('Company', companySchema)
