const Login = require('../models/Login')
const LoginController = {
  async addUser (req, res, next) {
    const payload = req.body
    const user = new Login(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      const user = await Login.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Login.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await Login.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Login.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = LoginController
