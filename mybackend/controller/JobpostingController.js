const Jobposting = require('../models/Jobposting')
const jobpostingController = {
  async addUser (req, res, next) {
    const payload = req.body
    const user = new Jobposting(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      const user = await Jobposting.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Jobposting.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await Jobposting.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Jobposting.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getSearch (req, res, next) {
    let { type, province, response } = req.body
    if (type === null) type = ''
    const jobposting = await Jobposting.find({
      $and: [
        {
          jobtype: { $regex: type, $options: 'i' }
        },
        {
          provinces: { $regex: province, $options: 'i' }
        },
        {
          responsibility: { $regex: response, $options: 'i' }
        }
      ]
    })
    res.status(200).json(jobposting)
  }
}
module.exports = jobpostingController
