import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/company',
    name: 'Company',
    component: () => import('../views/Compa/Company.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login/Login.vue')
  },
  {
    path: '/jobseeker',
    name: 'Jobseeker',
    component: () => import('../views/JobS/Jobseeker.vue')
  },
  {
    path: '/jobposting',
    name: 'Jobposting',
    component: () => import('../views/jobposting/jobposting.vue')
  },
  {
    path: '/signinCompa',
    name: 'SigninCompa',
    component: () => import('../views/Signin/signinCompa.vue')
  },
  {
    path: '/signinSeeker',
    name: 'SigninSeeker',
    component: () => import('../views/Signin/signinSeeker.vue')
  },
  {
    path: '/signOut',
    name: 'SignOut',
    component: () => import('../views/SignOut/signOut.vue')
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search/Search.vue')
  },
  {
    path: '/confirm',
    name: 'Confirm',
    component: () => import('../views/Confirm/confirm.vue')
  },
  {
    path: '/showdetail',
    name: 'Showdetail',
    component: () => import('../views/Showdetail/showdetail.vue')
  },
  {
    path: '/registerjobseeker',
    name: 'RegisterJ',
    component: () => import('../views/Register/Registerjobseeker.vue')
  },
  {
    path: '/registercompany',
    name: 'RegisterC',
    component: () => import('../views/Register/RegisterCompany.vue')
  },
  {
    path: '/successregister',
    name: 'SuccessRegister',
    component: () => import('../views/success/SuccessRegister.vue')
  },
  {
    path: '/resume',
    name: 'Resume',
    component: () => import('../views/Resume/Resume.vue')
  },
  {
    path: '/homejobseeker',
    name: 'Homejobseeker',
    component: () => import('../components/HeaderJobseeker.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
